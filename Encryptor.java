public class Encryptor {

	private byte[] ivbytes;
	private SecretKeySpec skeySpec;
	private Cipher cipher;
	private SecureRandom sr;
	private static final int ivlength = 16;

	public Encryptor(){
		// costruisco l'encryptor con una password di default
		this("PlatypusLLC54321");
	}
	
	public Encryptor(String key) {
		sr = new SecureRandom();
		// prima inizializzazione dell' IV
		ivbytes = new byte[ivlength];
		sr.nextBytes(ivbytes);

		try {
			// creo il cipher con l'algoritmo scelto
			skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private byte[] encrypt(byte[] data) {
		try {
			// inizializzo il chiper con l'InitializationVector
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(ivbytes));
			
			// ritorno i dati cifrati
			return cipher.doFinal(data);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	private byte[] decrypt(byte[] encrypted, byte[] ivbytes) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(ivbytes));
			
			// ritorno i dati decifrati
			return cipher.doFinal(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
			// se ho problemi con la decifratura ritorno i dati originali
			return encrypted;
		}
	}

	public byte[] encryptMessage(byte[] message){
		synchronized(this){
	        byte[] encrypted_data = encrypt(message);
			
			// creo i dati da inserire nel pacchetto: IV + dati cifrati
			byte[] result_data = new byte[ivlength + encrypted_data.length];
			System.arraycopy(ivbytes, 0, result_data, 0, ivlength);
			System.arraycopy(encrypted_data, 0, result_data, ivlength, encrypted_data.length);
			
			// dopo l'inizializzazione aggiorno l'IV con bytes random
			sr.nextBytes(ivbytes);

	        return result_data;
		}
	}

	public byte[] decryptMessage(byte[] message, int message_length){
		synchronized(this){
			// estraggo l' InitializationVector e i dati cifrati
			byte[] ivbytes = Arrays.copyOfRange(message, 0, ivlength);
			byte[] encrypted = Arrays.copyOfRange(message, ivlength, message_length);
			
			// ritorno i dati decifrati
			return decrypt(encrypted, ivbytes);
		}
	}
}
